class UsersController < ApplicationController
  before_action :logged_in_user , only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  def index
    #Only show acivated users
     @users = User.where(activated: true).paginate(page: params[:page])
  end
  def new
  	@user = User.new
  end
  def show
  	@user = User.find(params[:id])
    redirect_to root_path unless @user.activated?
  end
  def create
    @user = User.new(user_params)    
    if @user.save
      # Handle a successful save.
      # flash[:success] = "Welcome to the Sample App!"
      # log_in @user
      # remember @user
      # redirect_to @user
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "Update successfully"
      redirect_to @user
    else
      render 'edit'
    end    
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Delete successfully"
    redirect_to users_url
  end

  private
	  def user_params
	  	params.require(:user).permit(:name, :email, :password, :password_confirmation)
	  	#This code returns a version of the params hash with only the permitted attributes 
	  	#(while raising an error if the :user attribute is missing).
	  end

    def logged_in_user
      if !logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_path
      end
    end
    def correct_user
      @user = User.find(params[:id])
      if !correct_user?(@user)
        flash[:danger] = "You are not allowed to change this sort of information"
        redirect_to root_path
      end
    end

    def admin_user
      redirect_to root_path unless current_user.admin?
    end
end
